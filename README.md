## full_k65v1_64_bsp-user 10 QP1A.190711.020 mp1V91221 release-keys
- Manufacturer: alps
- Platform: mt6765
- Codename: k65v1_64_bsp
- Brand: alps
- Flavor: full_k65v1_64_bsp-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: mp1V91221
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: alps/full_k65v1_64_bsp/k65v1_64_bsp:10/QP1A.190711.020/mp1V91221:user/release-keys
- OTA version: 
- Branch: full_k65v1_64_bsp-user-10-QP1A.190711.020-mp1V91221-release-keys
- Repo: alps_k65v1_64_bsp_dump_15593


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
