#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:e48629d29eec0b905fa229f5583318bdbf58a5ff; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:6db21ef1f856e157b61c3e9df181cb5bf623c2c2 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:e48629d29eec0b905fa229f5583318bdbf58a5ff && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
